Radar Fakeimage detector
======

About
-----

Fake image detector combine the implementation of error-level analysis (ELA) and deep learning to detect whether an image has undergone fabrication or/and editing process or not, e.g. splicing. Accuracy is
99.17% based on my dataset (https://www.kaggle.com/sophatvathana/casia-dataset).


Requirements
------------

-   Python3.5+

Usage
-----

via Python Module

``` {.sourceCode .python}
from fakeimageradar import Fakeimageradar

detector = Fakeimageradar()
real = predict(<Image Array>)[0]
fake = predict(<Image Array>)[1]
```
