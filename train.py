import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import itertools
import os
import re

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping
from tensorflow.keras.optimizers import Adam
from livelossplot.inputs.tf_keras import PlotLossesCallback
from PIL import Image, ImageChops, ImageEnhance
from pylab import *


np.random.seed(2)

def convert_to_ela_image(path, quality):
    filename = path
    resaved_filename = 'tempresaved.jpg'
    ELA_filename = 'tempela.png'

    im = Image.open(filename).convert('RGB')
    im.save(resaved_filename, 'JPEG', quality = quality)
    resaved_im = Image.open(resaved_filename)

    ela_im = ImageChops.difference(im, resaved_im)
    
    extrema = ela_im.getextrema()
    max_diff = max([ex[1] for ex in extrema])
    if max_diff == 0:
        max_diff = 1
    scale = 255.0 / max_diff
    
    ela_im = ImageEnhance.Brightness(ela_im).enhance(scale)
    
    return ela_im

def train():
    X = []
    Y = []

    print('Convert ELA image original')
    for i in os.listdir('Original'):
        X.append(array(convert_to_ela_image('Original/'+i, 90).resize((128, 128))).flatten() / 255.0)
        Y.append(0)
    print('Finished convert ELA image original')
    print('Convert ELA image spliced')
    for i in os.listdir('Spliced'):
        X.append(array(convert_to_ela_image('Spliced/'+i, 90).resize((128, 128))).flatten() / 255.0)
        Y.append(1)
    print('Finished convert ELA image Spliced')

    print('Converting Array')
    X = np.array(X)
    Y = np.array(Y)
    print('Finished converting array')

    print('Reshaping X,Y')
    X = X.reshape(-1, 128, 128, 3)
    Y = Y.reshape(-1)
    print('Finished reshaping X,Y')

    print('Start training')
    X_train, X_val, Y_train, Y_val = train_test_split(X, Y, test_size = 0.2, random_state=5)
    print('Training finished')

    print('Defining CNN')
    model = Sequential()
    model.add(Conv2D(filters = 32, kernel_size = (5,5),padding = 'valid', 
                    activation ='relu', input_shape = (128,128,3)))
    model.add(Conv2D(filters = 32, kernel_size = (5,5),padding = 'valid', 
                    activation ='relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(256, activation = "relu"))
    model.add(Dropout(0.5))
    model.add(Dense(2, activation = "softmax"))

    print('Compiling')
    model.compile(optimizer = Adam() , loss = "sparse_categorical_crossentropy", metrics=["accuracy"])

    print('Generating checkpoint')
    checkpoint = ModelCheckpoint('FakeImgDetect.h5', monitor='loss', verbose=0,
        save_best_only=True, save_weights_only=True, mode='auto', save_freq='epoch')
    
    history = model.fit(X, Y, batch_size = 8, epochs = 50, verbose = 2, callbacks=[PlotLossesCallback(), checkpoint])

train()